﻿select concat(cpm.empresa, cpf.nombre) as cliente ,dv.cantidad, p.unidad_medida, p.descripcion, dv.precio_unitario, v.fecha_y_hora, c.tipo
from ventas v
left join detalles_de_ventas dv on v.folio = dv.folio
left join productos p on p.codigo_producto = dv.codigo_producto
left join clientes c on v.codigo_cliente = c.codigo_cliente
left join clientes_personas_morales cpm on c.codigo_cliente = cpm.codigo_cliente
left join clientes_personas_fisicas cpf on c.codigo_cliente = cpf.codigo_cliente
where c.tipo = 'Fisico' and v.codigo_cliente = 3 and date(v.fecha_y_hora) >= date('2015-10-26') and date(v.fecha_y_hora) <= date('2015-12-26') ;




ventas v on v., detalles_de_ventas dv,clientes_personas_morales cm, clientes_personas_fisicas cf, clientes c



--inner join clientes c on c.codigo_cliente = v.codigo_cliente
inner join clientes_personas_fisicas cf on c.codigo_cliente = cf.codigo_cliente
inner join clientes_personas_morales cm on c.codigo_cliente = cm.codigo_cliente
where cm.empresa = 'Sigma'

select c.codigo_cliente, concat(cf.nombre, ' ', cf.apellido_paterno, ' ', cf.apellido_materno) as nombreCombo , cm.empresa from clientes_personas_fisicas cf
inner join clientes c on c.codigo_cliente = cf.codigo_cliente
inner join clientes_personas_morales cm on c.codigo_cliente = cm.codigo_cliente
where c.codigo_cliente = 4

select * from clientes_personas_morales
c.estado = (
	select concat(cf.nombre, ' ', cf.apellido_paterno, ' ', cf.apellido_materno) as nombreCombo from clientes_personas_fisicas cf 
	inner join clientes c on c.codigo_cliente = cf.codigo_cliente where cf.status = TRUE 
	union
	select cm.empresa from clientes_personas_morales cm 
	inner join clientes c on c.codigo_cliente = cm.codigo_cliente where cm.status = TRUE )

	
inner join detalles_de_ventas dv on p.codigo_producto = dv.codigo_producto
inner join ventas v on v.folio = dv.folio
inner join clientes c on c.codigo_cliente = v.codigo_cliente
inner join clientes_personas_fisicas cf on c.codigo_cliente = cf.codigo_cliente
inner join clientes_personas_morales cm on c.codigo_cliente = cm.codigo_cliente
--where v.fecha_y_hora = '2015-10-26 19:50:27.258-06'

where c.estado in (select concat(cf.nombre, ' ', cf.apellido_paterno, ' ', cf.apellido_materno) as nombreCombo from clientes_personas_fisicas cf 
		inner join clientes c on c.codigo_cliente = cf.codigo_cliente where cf.status = TRUE 
		union
		select cm.empresa from clientes_personas_morales cm 
		inner join clientes c on c.codigo_cliente = cm.codigo_cliente 
		where cm.status = TRUE )
	and v.fecha_y_hora = '2015-10-26 19:50:27.258-06'

select concat(cf.nombre, ' ', cf.apellido_paterno, ' ', cf.apellido_materno) as nombreCombo from clientes_personas_fisicas cf 
		inner join clientes c on c.codigo_cliente = cf.codigo_cliente where cf.status = TRUE 
	union
	select cm.empresa from clientes_personas_morales cm 
		inner join clientes c on c.codigo_cliente = cm.codigo_cliente where cm.status = TRUE ;
	

--********************************************************************************************************************
select concat(cf.nombre, ' ', cf.apellido_paterno, ' ', cf.apellido_materno) as nombreCombo from clientes_personas_fisicas cf 
		inner join clientes c on c.codigo_cliente = cf.codigo_cliente where cf.status = TRUE 
union
select cm.empresa from clientes_personas_morales cm 
		inner join clientes c on c.codigo_cliente = cm.codigo_cliente where cm.status = TRUE ;


--********************************************************************************************************************



create or replace function fn_venta(_fecha date)
returns character varying as
$$
declare _nombre character varying;
begin 

select concat(cf.nombre, ' ', cf.apellido_paterno, ' ', cf.apellido_materno) as nombreCombo into _nombre from clientes_personas_fisicas cf 
		inner join clientes c on c.codigo_cliente = cf.codigo_cliente where cf.status = TRUE 
		union
		select cm.empresa from clientes_personas_morales cm 
		inner join clientes c on c.codigo_cliente = cm.codigo_cliente where cm.status = TRUE;


	select dv.cantidad, p.unidad_medida, _nombre, p.descripcion, dv.precio_unitario, v.fecha_y_hora from productos p
inner join detalles_de_ventas dv on p.codigo_producto = dv.codigo_producto
inner join ventas v on v.folio = dv.folio
inner join clientes c on c.codigo_cliente = v.codigo_cliente		
where v.fecha_y_hora = _fecha ;
return _nombre;

end;
$$
language plpgsql;

drop function fn_venta(date); 

select * from fn_venta('2015-10-26 19:50:27.258-06');