﻿
----------------------------------------------------------Restaurar Reciclaje Productos-----------------------------------------------------
--select * from productos;

create or replace function fn_restaurar_productos (_idProducto integer)
returns void as
$body$
begin
	update productos set status = 't' where codigo_producto = _idProducto; 
end
$body$
language plpgsql;

--select fn_restaurar_productos (7);

-----------------------------------------------------------Restaurar Reciclaje Usuario----------------------------------------------------------
--select * from usuarios where status = 'f'

create or replace function fn_restaurar_usuarios(_idUsuario integer)
returns void as
$body$
begin
	update usuarios set status = 't' where codigo_usuario = _idUsuario;
end
$body$
language plpgsql;

---------------------------------------------------------Restaurar Reciclaje Clientes-------------------------------------------------------------
--select * from clientes_personas_morales
--select concat(nombre, ' ', apellido_paterno, ' ', apellido_materno) as nombreCombo from clientes_personas_fisicas cf where cf.status = FALSE UNION select empresa from clientes_personas_morales cm where cm.status = FALSE

create or replace function fn_restaurar_clientes(_idClientes integer)
returns void as
$body$
begin
	update clientes_personas_fisicas set status = 't' where codigo_cliente = _idClientes;
	
	update clientes_personas_morales set status = 't' where codigo_cliente = _idClientes;
end
$body$
language plpgsql;

--select fn_restaurar_clientes (4)

-------------------------------------------------------Restaurar Reciclaje Proveedores-----------------------------------------------------------
--select * from proveedores
create or replace function fn_restaurar_proveedores(_idProveedor integer)
returns void as
$body$
begin
	update proveedores set status = 't' where codigo_proveedor = _idProveedor;
	update contactos_proveedores set status = 't' where codigo_proveedor = _idProveedor;
	
end
$body$
language plpgsql;

select fn_restaurar_proveedores (1);