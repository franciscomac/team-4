
package modelo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import controlador.ControladorErrores;
import controlador.Encriptar;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class Conexion {

	// #region Atributos
	private String					servidor, usuario, contrasenia, bd, ip, puerto;
	private static Conexion			instancia;
	private Connection				con;
	private ControladorErrores		error;
	public static String			base;
	private File					archivo;
	private BufferedWriter			escribir;
	private static BufferedReader	leer;
	private static Encriptar		encriptar;
	// #endregion

	/*
	 * Constructor sin param�tros que permitir� definir un dato por default de
	 * la clase
	 */
	// #region constructor
	private Conexion() {
		error = new ControladorErrores();
		bd = "rossma";
		usuario = "postgres";
		contrasenia = "root";
		servidor = "jdbc:postgresql://";
		puerto = "5432";
		ip = "127.0.0.1";
		con = null;
		base = System.getProperty("user.dir") + "\\src\\vista\\Base_de_datos\\base.rossma";
		archivo = new File(base);
		encriptar = new Encriptar();
	}
	// #endregion

	/*
	 * Constructor con par�metros que permita inicializar con valores
	 * personalizados
	 */
	// #region constructor con parametros
	private Conexion(String usuario, String contrasenia, String bd, String puerto, String ip) {
		this.servidor = "jdbc:postgresql://";
		this.contrasenia = contrasenia;
		this.usuario = usuario;
		this.bd = bd;
		this.ip = ip;
		this.puerto = puerto;
		con = null;
	}
	// #endregion

	/*
	 * M�todo para recuperar la instancia de la clase de conexi�n
	 */
	public static Conexion getInstancia(){

		if( instancia == null ) {
			// Inicializar
			instancia = new Conexion();
		}
		return instancia;
	}

	/*
	 * M�todo para conectar al servidor Postgresql
	 */
	public String conectar(){

		try {
			Class.forName("org.postgresql.Driver");
			leer = new BufferedReader(new FileReader(base));
			int contador = 0;
			String linea;
			String password = "";
			String user = "";
			String configuracion = "";
			while( (linea = leer.readLine()) != null ) {
				contador++;
				if( contador == 1 ) {
					configuracion = encriptar.desencriptar(linea, "rossma");
				}
				else
					if( contador == 2 ) {
						configuracion += encriptar.desencriptar(linea, "rossma");
					}
					else
						if( contador == 3 ) {
							configuracion += encriptar.desencriptar(linea, "rossma");
						}
						else
							if( contador == 4 ) {
								configuracion += encriptar.desencriptar(linea, "rossma");
							}
							else
								if( contador == 5 ) {
									configuracion += encriptar.desencriptar(linea, "rossma");
								}
								else
									if( contador == 6 ) {
										configuracion += encriptar.desencriptar(linea, "rossma");
									}
									else
										if( contador == 7 ) {
											password = encriptar.desencriptar(linea, "rossma");
										}
										else
											if( contador == 8 ) {
												user = encriptar.desencriptar(linea, "rossma");
											}
			}
			leer.close();
			con = DriverManager.getConnection(configuracion, user, password);
			return "Conexi�n �xitosa";
		}
		catch( Exception ex ) {
			error.printLong(ex.getMessage(), this.getClass().toString());
			return "No se establecio la conexi�n. Consulte a su administrador.";
		}
	}

	public boolean verificarBase(){

		try {
			Class.forName("org.postgresql.Driver");
			leer = new BufferedReader(new FileReader(base));
			int contador = 0;
			String linea;
			String password = "";
			String user = "";
			String configuracion = "";
			while( (linea = leer.readLine()) != null ) {
				contador++;
				if( contador == 1 ) {
					configuracion = encriptar.desencriptar(linea, "rossma");
				}
				else
					if( contador == 2 ) {
						configuracion += encriptar.desencriptar(linea, "rossma");
					}
					else
						if( contador == 3 ) {
							configuracion += encriptar.desencriptar(linea, "rossma");
						}
						else
							if( contador == 4 ) {
								configuracion += encriptar.desencriptar(linea, "rossma");
							}
							else
								if( contador == 5 ) {
									configuracion += encriptar.desencriptar(linea, "rossma");
								}
								else
									if( contador == 7 ) {
										password = encriptar.desencriptar(linea, "rossma");
									}
									else
										if( contador == 8 ) {
											user = encriptar.desencriptar(linea, "rossma");
										}
			}
			leer.close();
			con = DriverManager.getConnection(configuracion, user, password);
			return true;
		}
		catch( Exception ex ) {
			error.printLong(ex.getMessage(), this.getClass().toString());
			return true;
		}
	}

	public boolean borrarFichero(){

		if( archivo.delete() ) {
			return true;
		}
		return false;
	}
	// SELECT datname FROM pg_database where datname ='rossma'

	// public static void main(String[] args){
	//// Conexion conec = new Conexion();
	//// conec.reestablecerDatosDeConexion();
	//
	// try {
	// Conexion conec = new Conexion();
	// conec.verificarBase();
	// int contador = 0;
	// String linea = "";
	// String basee = "";
	// leer = new BufferedReader(new FileReader(base));
	// while ((linea= leer.readLine()) != null) {
	// contador++;
	// if (contador == 6) {
	// basee += encriptar.desencriptar(linea, "rossma");
	// }
	// }
	//
	// PreparedStatement query = conec.getConexion().prepareStatement("SELECT
	// datname FROM pg_database where datname = ?");
	// query.setString(1, basee);
	// ResultSet rs = query.executeQuery();
	// if (rs.next()) {
	// System.out.println("si lo hiso");
	// System.out.println("DB Name : " + rs.getString(1));
	// }else {
	//
	// }
	//
	// } catch (SQLException | IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }
	public void reestablecerDatosDeConexion(boolean remplazar){

		try {
			if( !remplazar ) {
				if( !archivo.exists() ) {
					escribir = new BufferedWriter(new FileWriter(base));
					escribir.write(encriptar.encriptar(servidor, "rossma"));
					escribir.newLine();
					escribir.write(encriptar.encriptar(ip, "rossma"));
					escribir.newLine();
					escribir.write(encriptar.encriptar(":", "rossma"));
					escribir.newLine();
					escribir.write(encriptar.encriptar(puerto, "rossma"));
					escribir.newLine();
					escribir.write(encriptar.encriptar("/", "rossma"));
					escribir.newLine();
					escribir.write(encriptar.encriptar(bd, "rossma"));
					escribir.newLine();
					escribir.write(encriptar.encriptar(contrasenia, "rossma"));
					escribir.newLine();
					escribir.write(encriptar.encriptar(usuario, "rossma"));
					escribir.close();
				}
			}
			else {
				escribir = new BufferedWriter(new FileWriter(base));
				escribir.write(encriptar.encriptar(servidor, "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar(ip, "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar(":", "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar(puerto, "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar("/", "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar(bd, "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar(contrasenia, "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar(usuario, "rossma"));
				escribir.close();
			}
		}
		catch( IOException e ) {
			// TODO Auto-generated catch block
			System.out.println("Error al cargar o leer el fichero");
		}
	}

	/*
	 * M�todo para desconectar del servidor de Postgresql
	 */
	public String desconectar(){

		try {
			// Cerrar la conexi�n
			con.close();
			return "Se ha desconectado del servidor";
		}
		catch( Exception ex ) {
			// TODO: handle exception
			ex.printStackTrace();
			error.printLong(ex.getMessage(), this.getClass().toString());
			return "La conexi�n est� siendo ocupada. No se puede desconectar.";
		}
	}

	/*
	 * M�todo para recuperar la conexion abierta
	 */
	public static void main(String[] args){

		Conexion c = new Conexion();
		c.reestablecerDatosDeConexion(true);
	}

	public Connection getConexion(){

		return con;
	}

	// #region
	public String getUsuario(){

		return usuario;
	}

	public void setUsuario(String usuario){

		this.usuario = usuario;
	}

	public String getContrasenia(){

		return contrasenia;
	}

	public void setContrasenia(String contrasenia){

		this.contrasenia = contrasenia;
	}

	public String getBd(){

		return bd;
	}

	public void setBd(String bd){

		this.bd = bd;
	}

	public String getIp(){

		return ip;
	}

	public void setIp(String ip){

		this.ip = ip;
	}

	public String getPuerto(){

		return puerto;
	}

	public void setPuerto(String puerto){

		this.puerto = puerto;
	}
	// #endregion
}
