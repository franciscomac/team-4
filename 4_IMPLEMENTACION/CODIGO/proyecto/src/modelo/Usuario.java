
package modelo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import controlador.Encriptar;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class Usuario {

	// #region Atributos
	private String				nombre, contrasenia, privilegio;
	public static Integer		codigo;
	private Boolean				estatus;
	private Conexion			con;
	public String				base;
	private BufferedReader		leer;
	private Encriptar			encriptar;
	/*
	 * Objeto de conexion
	 */
	private Connection			miconexion;
	/*
	 * Para ejecutar una instruccion SQL
	 */
	private PreparedStatement	comando;
	// #endregion

	// #region Constructor
	public Usuario() {
		this.nombre = null;
		this.contrasenia = null;
		this.privilegio = null;
		this.estatus = null;
		con = con.getInstancia();
		base = System.getProperty("user.dir") + "\\src\\vista\\Base_de_datos\\base.rossma";
		// this.codigo=null;
		encriptar = new Encriptar();
	}
	// #endregion

	// #region Getters and Setters
	public String getNombre(){

		return nombre;
	}

	public void setNombre(String nombre){

		this.nombre = nombre;
	}

	public String getContrasenia(){

		return contrasenia;
	}

	public void setContrasenia(String contrasenia){

		this.contrasenia = contrasenia;
	}

	public String getPrivilegio(){

		return privilegio;
	}

	public void setPrivilegio(String privilegio){

		this.privilegio = privilegio;
	}

	public Boolean getEstatus(){

		return estatus;
	}

	public void setEstatus(Boolean estatus){

		this.estatus = estatus;
	}
	// #endregion
	// #region M�todos de acceso a datos
	/*
	 * M�todo para verificar que existen en la base de datos.
	 */

	public boolean Existe(){

		con = Conexion.getInstancia();
		boolean bandera = false;
		try {
			String sql = "select tipo,nombre,codigo_usuario from usuarios where usuario=? and" + " contrasenia=? and" + " status = TRUE";
			// Abrir conexion
			con.conectar();
			// Se recupera la conexion
			// miconexion= con.getConexion();
			// Asociamos el comando con la conexion
			comando = con.getConexion().prepareStatement(sql);
			// Par�metros
			comando.setString(1, this.nombre);
			comando.setString(2, this.contrasenia);
			// Se ejecuta la instruccion
			// ResultSet es una tabla temporal
			ResultSet rs = comando.executeQuery();
			// Validar los datos
			// Si existen datos en el resultset
			while( rs.next() ) {
				this.privilegio = (rs.getString("tipo"));
				this.nombre = (rs.getString("nombre"));
				codigo = (rs.getInt("codigo_usuario"));
				bandera = true;
			}
			rs.close();
		}
		catch( Exception e ) {
			System.out.println("*********Error*********");
			e.printStackTrace();
		}
		finally {
			// El bloque Finally se ejecuta exista o no un error.
			con.desconectar();
		}
		return bandera;
	}

	public boolean baseExiste(){

		boolean bandera = false;
		try {
			con.getInstancia();
			con.verificarBase();
			int contador = 0;
			String linea = "";
			String basee = "";
			leer = new BufferedReader(new FileReader(base));
			while( (linea = leer.readLine()) != null ) {
				contador++;
				if( contador == 6 ) {
					System.out.println(encriptar.desencriptar(linea, "rossma"));
					basee = encriptar.desencriptar(linea, "rossma");
					break;
				}
			}
			leer.close();
			PreparedStatement query = con.getConexion().prepareStatement("SELECT datname FROM pg_database where datname = ?");
			query.setString(1, basee);
			ResultSet rs = query.executeQuery();
			if( rs.next() ) {
				System.out.println("DB Name : " + rs.getString(1));
				bandera = false;
			}
			else {
				int reply = JOptionPane.showConfirmDialog(null, "Desea intentar restaurarla", "Base de datos no encontrada", JOptionPane.YES_NO_OPTION);
				bandera = true;
			}
		}
		catch( SQLException | IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			con.desconectar();
		}
		return bandera;
	}
	// #endregion
}
