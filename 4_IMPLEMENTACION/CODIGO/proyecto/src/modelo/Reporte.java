
package modelo;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import controlador.ControladorErrores;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class Reporte {

	private ControladorErrores	error;
	private JasperDesign		dise�o;
	private JasperPrint			impreso;
	private JasperReport		reporte;
	Conexion					con;

	public Reporte() {
		dise�o = new JasperDesign();
		impreso = new JasperPrint();
		con = Conexion.getInstancia();
		error = new ControladorErrores();
	}

	public void cargarReporte(String ruta){

		try {
			File f = new File(ruta);
			dise�o = JRXmlLoader.load(f.getAbsolutePath());
			reporte = JasperCompileManager.compileReport(dise�o);
			con.conectar();
			impreso = JasperFillManager.fillReport(reporte, new HashMap<>(), con.getConexion());
			con.desconectar();
		}
		catch( Exception e ) {
			// TODO: handle exception
		}
	}

	public void cargarCliente(String ruta, String tipo, Boolean status){

		try {
			File f = new File(ruta);
			dise�o = JRXmlLoader.load(f.getAbsolutePath());
			reporte = JasperCompileManager.compileReport(dise�o);
			con.conectar();
			HashMap<String, Object> parametros = new HashMap<>();
			parametros.put("tipo", tipo);
			parametros.put("status", status);
			// boolean a = parametros.isEmpty();
			impreso = JasperFillManager.fillReport(reporte, parametros, con.getConexion());
			con.desconectar();
		}
		catch( Exception e ) {
			// TODO: handle exception
		}
	}

	public void cargarProveedore(String ruta, Boolean status, String contacto, String estado){

		try {
			File f = new File(ruta);
			dise�o = JRXmlLoader.load(f.getAbsolutePath());
			reporte = JasperCompileManager.compileReport(dise�o);
			con.conectar();
			HashMap<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("statusProveedor", status);
			parametros.put("contacto", contacto);
			parametros.put("estado", estado);
			System.out.println(status + contacto + estado);
			// boolean a = parametros.isEmpty();
			// System.out.println(a);
			impreso = JasperFillManager.fillReport(reporte, parametros, con.getConexion());
			con.desconectar();
		}
		catch( Exception e ) {
			e.getMessage();
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	public void cargarVentas(String ruta, Integer codigoCliente, String fechaInicio, String fechaFinal){

		try {
			File f = new File(ruta);
			dise�o = JRXmlLoader.load(f.getAbsolutePath());
			reporte = JasperCompileManager.compileReport(dise�o);
			con.conectar();
			System.out.println("paso1");
			Map<String, Object> parametros = new HashMap<>();
			System.out.println("paso2");
			// parametros.put("Tipo", tipo );
			parametros.put("codigoCliente", codigoCliente);
			parametros.put("fechaInicio", fechaInicio);
			parametros.put("fechaFinal", fechaFinal);
			// boolean a = parametros.isEmpty();
			impreso = JasperFillManager.fillReport(reporte, parametros, con.getConexion());
			con.desconectar();
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}
	
	public void cargarVentasTodos(String ruta,  String fechaInicio, String fechaFinal){

		try {
			File f = new File(ruta);
			dise�o = JRXmlLoader.load(f.getAbsolutePath());
			reporte = JasperCompileManager.compileReport(dise�o);
			con.conectar();
			System.out.println("paso1");
			Map<String, Object> parametros = new HashMap<>();
			System.out.println("paso2");
			// parametros.put("Tipo", tipo );
			//parametros.put("codigoCliente", codigoCliente);
			parametros.put("fechaInicio", fechaInicio);
			parametros.put("fechaFinal", fechaFinal);
			// boolean a = parametros.isEmpty();
			impreso = JasperFillManager.fillReport(reporte, parametros, con.getConexion());
			con.desconectar();
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}

	
	public void mostrarReporte(){

		try {
			System.out.println("dfghjkl");
			JasperViewer.viewReport(impreso, false);
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}
}
