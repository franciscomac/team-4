
package modelo;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import org.codehaus.groovy.tools.groovydoc.Main;
import controlador.ControladorErrores;
import controlador.Encriptar;
import controlador.LeerDatosCMD;
import vista.Splash.SplashJF2;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class CopiayRestaruacionBD {

	// #region atributos
	private String					ruta;
	public static String			base;
	private static BufferedReader	leer;
	private static Encriptar		encriptar;
	private Conexion				con;
	private File					archivo;
	private String					linea;
	private String					password;
	private String					user	= "";
	private String					configuracion;
	private String					ip;
	private String					puerto;
	private String					bd;
	private String					servidor;
	private ControladorErrores		error;
	// #endregion

	// #region
	public CopiayRestaruacionBD() {
		ruta = System.getProperty("user.dir") + "\\src\\vista\\Base_de_datos\\respaldo.backup";
		base = System.getProperty("user.dir") + "\\src\\vista\\Base_de_datos\\base.rossma";
		encriptar = new Encriptar();
		con = con.getInstancia();
		archivo = new File(base);
		linea = "";
		password = "";
		user = "";
		configuracion = "";
		ip = "";
		puerto = "";
		bd = "";
		error = new ControladorErrores();
	}
	// #endregion

	public void backup(){

		try {
			if( con.conectar().equals("Conexi�n �xitosa") ) {
				leer = new BufferedReader(new FileReader(base));
				int contador = 0;
				while( (linea = leer.readLine()) != null ) {
					contador++;
					if( contador == 2 ) {
						ip = encriptar.desencriptar(linea, "rossma");
					}
					else
						if( contador == 4 ) {
							puerto = encriptar.desencriptar(linea, "rossma");
						}
						else
							if( contador == 6 ) {
								bd = encriptar.desencriptar(linea, "rossma");
							}
							else
								if( contador == 8 ) {
									user = encriptar.desencriptar(linea, "rossma");
								}
				}
				leer.close();
				System.out.println("C:/Program Files/PostgreSQL/9.4/bin\\pg_dump.exe" + " --host " + "localhost" + " --port " + puerto + " --username \"" + user + "\" --no-password " + " --format custom " + " --blobs --verbose --file \"" + ruta + "\" \"" + bd + "\"");
				Process p = Runtime.getRuntime().exec("C:/Program Files/PostgreSQL/9.4/bin\\pg_dump.exe" + " --host " + "localhost" + " --port " + puerto + " --username \"" + user + "\" --no-password " + " --format custom " + " --blobs --verbose --file \"" + ruta + "\" \"" + bd + "\"");
				LeerDatosCMD salida = new LeerDatosCMD(p.getInputStream());
				LeerDatosCMD errordeSalida = new LeerDatosCMD(p.getErrorStream());
				new Thread(salida).start();
				new Thread(errordeSalida).start();
				p.waitFor();
			}
			else {
				System.out.println("fallo al conectar con la base de datos");
			}
		}
		catch( IOException | InterruptedException e ) {
			e.printStackTrace();
		}
	}

	public boolean baseExiste(){

		boolean bandera = false;
		try {
			con.getInstancia();
			con.verificarBase();
			int contador = 0;
			String basee = "";
			leer = new BufferedReader(new FileReader(base));
			while( (linea = leer.readLine()) != null ) {
				contador++;
				if( contador == 6 ) {
					basee = encriptar.desencriptar(linea, "rossma");
					break;
				}
			}
			leer.close();
			con.verificarBase();
			PreparedStatement query = con.getConexion().prepareStatement("SELECT datname FROM pg_database where datname = ?");
			query.setString(1, basee);
			ResultSet rs = query.executeQuery();
			if( rs.next() ) {
				bandera = false;
			}
			else {
				int decision = JOptionPane.showConfirmDialog(null, "Desea intentar restaurar la Base de Datos", "Base de datos no encontrada", JOptionPane.YES_NO_OPTION);
				if( decision == 0 ) {
					if( archivo.exists() ) {
						leer = new BufferedReader(new FileReader(base));
						int contador2 = 0;
						while( (linea = leer.readLine()) != null ) {
							contador2++;
							if( contador2 == 2 ) {
								ip = encriptar.desencriptar(linea, "rossma");
							}
							else
								if( contador2 == 4 ) {
									puerto = encriptar.desencriptar(linea, "rossma");
								}
								else
									if( contador2 == 6 ) {
										bd = encriptar.desencriptar(linea, "rossma");
									}
									else
										if( contador2 == 8 ) {
											user = encriptar.desencriptar(linea, "rossma");
										}
						}
						leer.close();
						PreparedStatement crearBase = con.getConexion().prepareStatement("CREATE DATABASE " + bd + " WITH ENCODING = 'UTF8' CONNECTION LIMIT = -1");
						int resultado = crearBase.executeUpdate();
						if( resultado == 0 ) {
							Process ps = Runtime.getRuntime().exec("C:\\Program Files\\PostgreSQL\\9.4\\bin\\pg_restore.exe" + " --host " + "localhost" + " --port " + puerto + " --username \"" + user + "\" --dbname \"" + bd + "\" --no-password  --verbose \"" + ruta + "\"");
							System.out.println("C:\\Program Files\\PostgreSQL\\9.4\\bin\\pg_restore.exe" + " --host " + "localhost" + " --port " + puerto + " --username \"" + user + "\" --dbname \"" + bd + "\" --no-password  --verbose \"" + ruta + "\"");
							LeerDatosCMD salidaa = new LeerDatosCMD(ps.getInputStream());
							LeerDatosCMD errordeSalidaa = new LeerDatosCMD(ps.getErrorStream());
							new Thread(salidaa).start();
							new Thread(errordeSalidaa).start();
							ps.waitFor();
							System.out.println("termino");
							bandera = true;
						}
						else {
							System.out.println("La base de datos no pudo ser creada");
							bandera = false;
						}
					}
					else {
						System.out.println("Lo sentimos el archivo de restauracion no existe");
						bandera = false;
					}
				}
				else {
					bandera = false;
				}
			}
		}
		catch( SQLException | IOException | InterruptedException e ) {
			error.printLong(e.getMessage(), this.getClass().toString());
			System.out.println("aki trono");
			e.printStackTrace();
		}
		finally {
			con.desconectar();
		}
		return bandera;
	}

	public static void main(String[] args){

		CopiayRestaruacionBD c = new CopiayRestaruacionBD();
		// c.backup();
		c.baseExiste();
	}
}