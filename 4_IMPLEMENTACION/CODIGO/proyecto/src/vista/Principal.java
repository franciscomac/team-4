
package vista;

import controlador.ControladorVentanas;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import modelo.CopiayRestaruacionBD;
import vista.Splash.SplashJF2;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class Principal extends Application {

	private static SplashJF2	frame	= new SplashJF2();
	private static Stage		primaryStage;
	private ControladorVentanas	ventanas;

	public static void main(String[] args){

		frame.setUndecorated(true);
		frame.setVisible(true);
		SplashJF2 s = new SplashJF2();
		System.out.println("antes");
		s.armartodo();
		System.out.println("despues");
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception{

		Principal.primaryStage = primaryStage;
		ventanas = ControladorVentanas.getInstancia();
		ventanas.setPrimaryStage(getPrimaryStage());
		ventanas.asignarEscena2("../vista/fxml/Login.fxml", "Login", primaryStage);
		// TODO Auto-generated method stub
		frame.setVisible(false);
		// Parent root =
		// FXMLLoader.load(getClass().getResource("fxml/Login.fxml"));
		// Scene escena = new Scene(root);
		// primaryStage.setTitle("Conexion");
		// primaryStage.setScene(escena);
		// primaryStage.show();
	}

	/**
	 * Returns the main stage.
	 * 
	 * @return
	 */
	public static Stage getPrimaryStage(){

		return primaryStage;
	}
}
