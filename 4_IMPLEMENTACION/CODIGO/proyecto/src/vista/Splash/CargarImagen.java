
package vista.Splash;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class CargarImagen extends JLabel {

	public CargarImagen() {
		setIcon(new ImageIcon(System.getProperty("user.dir") + "\\src\\vista\\imagenes\\final rossma.png"));
		setBackground(new Color(0, 0, 0, 0));
		setOpaque(false);
	}
}
