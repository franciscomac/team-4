
package controlador;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import modelo.Usuario;
import vista.Principal;

/**
 * 27 de sept. de 2015
 * 
 * @ROSSMA Fco. Javier Machuca Rojas H�ctor Ren� Espinoza G�mez Luis Angel
 *         Fernndez Vasquez
 */
public class CerrarSesion implements Initializable {

	// #region de variables
	private Principal				principal;
	private ControladorVentanas		conVentanas;
	private Usuario					modeloUsuario;
	private ControladorPrincipal	conPr;
	private BorderPane				contenedorDialog;
	private Button					btnNo;
	// #endregion

	// #region del constructor
	public CerrarSesion() {
		principal = new Principal();
		conVentanas = ControladorVentanas.getInstancia();
		modeloUsuario = new Usuario();
		conPr = new ControladorPrincipal();
		contenedorDialog = conVentanas.contenedorDialog;
	}
	// #endregion

	// #region del boton SI cerrar sesion
	@FXML
	public void cerrarSi() throws Exception{

		principal.start(principal.getPrimaryStage());
		principal.getPrimaryStage().centerOnScreen();
		conVentanas.dialogStage.close();
	}
	// #endregion

	// #region del boton NO cerrar sesion
	@FXML
	public void cerrarNo(){

		conVentanas.dialogStage.close();
		contenedorDialog.setEffect(null);
	}
	// #endregion

	@Override
	public void initialize(URL location, ResourceBundle resources){
		// TODO Auto-generated method stub

	}
}
