
package controlador;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import vista.Principal;
import vista.animacion.TransicionDerecha;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class ControladorVentanas {
	/*
	 * Atributos
	 */

	private static ControladorVentanas	instancia;
	private Stage						primaryStage;
	public static Stage					dialogStage;
	private BorderPane					contenedor;
	public static BorderPane			contenedorDialog;
	public static AnchorPane			contenedorDialog2;
	private AnchorPane					subContenedorDialog;
	private Scene						escena;
	private ControladorErrores			error;

	/*
	 * Contructor privado
	 */
	private ControladorVentanas() {
		error = new ControladorErrores();
	}

	/*
	 * Recuperar la Instancia de la clase
	 */
	public static ControladorVentanas getInstancia(){

		if( instancia == null ) {
			instancia = new ControladorVentanas();
		}
		return instancia;
	}

	/*
	 * Establecer Escenario Principal
	 */
	public void setPrimaryStage(Stage primaryStage){

		this.primaryStage = primaryStage;
	}

	public void asignarMenu(String ruta, String titulo){

		try {
			FXMLLoader interfaz = new FXMLLoader(getClass().getResource(ruta));
			contenedor = ( BorderPane ) interfaz.load();
			Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
			Scene escena = new Scene(contenedor, screenBounds.getWidth(), screenBounds.getHeight());
			primaryStage.setScene(escena);
			primaryStage.setTitle(titulo);
			primaryStage.show();
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}

	public void asignarEscena(String ruta, String titulo, String nivel, String nombre){

		try {
			ControladorPrincipal.nombre = nombre;
			ControladorPrincipal.nivel = nivel;
			FXMLLoader interfaz = new FXMLLoader(getClass().getResource(ruta));
			contenedorDialog = ( BorderPane ) interfaz.load();
			primaryStage = Principal.getPrimaryStage();
			primaryStage.setTitle(titulo);
			escena = new Scene(contenedorDialog);
			primaryStage.setScene(escena);
			primaryStage.centerOnScreen();
			// primaryStage.setResizable(false);
			primaryStage.show();
			primaryStage.setTitle("ROSSMA--- Usuario: " + nombre + "  Tipo: " + nivel);
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}

	public void asignarEscena2(String ruta, String titulo, Stage capa){

		try {
			FXMLLoader interfaz = new FXMLLoader(getClass().getResource(ruta));
			contenedorDialog2 = ( AnchorPane ) interfaz.load();
			primaryStage = capa;
			// primaryStage.setTitle(titulo);
			escena = new Scene(contenedorDialog2);
			primaryStage.setScene(escena);
			primaryStage.centerOnScreen();
			// primaryStage.setResizable(false);
			primaryStage.show();
			primaryStage.setTitle(titulo);
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}

	public void asignarCentro(String ruta){

		try {
			FXMLLoader interfaz = new FXMLLoader(getClass().getResource(ruta));
			subContenedorDialog = ( AnchorPane ) interfaz.load();
			contenedorDialog.setCenter(subContenedorDialog);
			new TransicionDerecha(subContenedorDialog).play();
			// Scene escena = new Scene(contenedorDialog);
			primaryStage.setScene(escena);
			primaryStage.centerOnScreen();
			this.primaryStage.getScene().getRoot().setEffect(null);
			primaryStage.show();
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}

	public void asignarEmergente(String ruta, String titulo){

		try {
			FXMLLoader emergente = new FXMLLoader(getClass().getResource(ruta));
			AnchorPane page = ( AnchorPane ) emergente.load();
			dialogStage = new Stage();
			dialogStage.setTitle(titulo);
			dialogStage.initStyle(StageStyle.UNDECORATED);
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			if( !(contenedorDialog == null) ) {
				contenedorDialog.setEffect(new GaussianBlur(10));
			}
			else {
				contenedorDialog2.setEffect(new GaussianBlur(10));
			}
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.show();
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}

	public Stage getPrimaryStage(){

		return primaryStage;
	}
}
