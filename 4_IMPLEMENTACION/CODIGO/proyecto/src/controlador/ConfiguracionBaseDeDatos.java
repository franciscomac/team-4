
package controlador;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.security.Principal;
import java.util.ResourceBundle;
import groovy.util.IFileNameFinder;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import modelo.Conexion;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class ConfiguracionBaseDeDatos implements Initializable {

	// #region atributos
	@FXML
	private Label				lblMensajes, lblTitulo;
	@FXML
	private TextField			txtIp, txtPuerto, txtBasedeDatos, txtUsuario, txtContrasenia;
	@FXML
	private Button				btbconectar, btnSesion, btnDefault;
	private Conexion			con;
	private File				archivo;
	private BufferedWriter		escribir;
	private BufferedReader		leer;
	private Encriptar			encriptar;
	public static String		base;
	private ControladorVentanas	ventanas;
	// #endregion

	// #region contructor
	public ConfiguracionBaseDeDatos() {
		base = System.getProperty("user.dir") + "\\src\\vista\\Base_de_datos\\base.rossma";
		archivo = new File(base);
		encriptar = new Encriptar();
		con = con.getInstancia();
		ventanas = ControladorVentanas.getInstancia();
	}
	// #endregion

	@FXML
	public void conectar(){

		try {
			if( validarCampos() ) {
				escribir = new BufferedWriter(new FileWriter(base));
				escribir.write(encriptar.encriptar("jdbc:postgresql://", "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar(txtIp.getText(), "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar(":", "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar(txtPuerto.getText(), "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar("/", "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar(txtBasedeDatos.getText(), "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar(txtContrasenia.getText(), "rossma"));
				escribir.newLine();
				escribir.write(encriptar.encriptar(txtUsuario.getText(), "rossma"));
				escribir.close();
				lblMensajes.setText(con.conectar());
			}
		}
		catch( IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			con.desconectar();
		}
	}

	public boolean validarCampos(){

		if( txtIp.getText().trim().isEmpty() ) {
			lblMensajes.setText("Campo ip vacio");
			return false;
		}
		else
			if( txtPuerto.getText().trim().isEmpty() ) {
				lblMensajes.setText("Campo puerto vacio");
				return false;
			}
			else
				if( txtBasedeDatos.getText().trim().isEmpty() ) {
					lblMensajes.setText("Campo base de datos vacio");
					return false;
				}
				else
					if( txtUsuario.getText().trim().isEmpty() ) {
						lblMensajes.setText("Campo usuario vacio");
						return false;
					}
					else
						if( txtContrasenia.getText().trim().isEmpty() ) {
							lblMensajes.setText("Campo contrase�a vacio");
							return false;
						}
						else {
							return true;
						}
	}

	@FXML
	public void cerrarSesion(){

		Stage primaryStage = ventanas.getPrimaryStage();
		ventanas.dialogStage.close();
		ventanas.asignarEscena2("../vista/fxml/Login.fxml", "Login", primaryStage);
	}

	@FXML
	public void restableser(){

		archivo.delete();
		con.reestablecerDatosDeConexion(false);
		lblMensajes.setText(con.conectar());
		con.desconectar();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources){
		// TODO Auto-generated method stub

	}
}
