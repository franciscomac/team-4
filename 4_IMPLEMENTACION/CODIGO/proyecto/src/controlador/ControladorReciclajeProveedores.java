
package controlador;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import modelo.NuevoUsuario;
import modelo.Proveedores;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class ControladorReciclajeProveedores implements Initializable {

	private Proveedores							pro;
	private ControladorVentanas					contVen;
	private ControladorErrores					error;
	@FXML
	private TextField							txtNombre;
	@FXML
	private Label								lblMensaje;
	@FXML
	private Button								btnRestaurar;
	@FXML
	private TableView<Proveedores>				tablaReciclar;
	@FXML
	private TableColumn<Proveedores, String>	tcNombre, tcCorreo, tcTelefono;
	private ObservableList<Proveedores>			reci;
	private ObservableList<Proveedores>			rest;

	public ControladorReciclajeProveedores() {
		pro = new Proveedores();
		error = new ControladorErrores();
		contVen = ControladorVentanas.getInstancia();
		reci = rest = FXCollections.observableArrayList();
	}

	public ObservableList<Proveedores> restaurar(){

		rest = FXCollections.observableArrayList();
		Proveedores p = new Proveedores();
		p.setId_proveedor(new SimpleIntegerProperty());
		p.setStatus(new Boolean(true));
		rest.add(p);
		return rest;
	}

	public void actualizarTabla(){

		try {
			pro = new Proveedores();
			reci = pro.getReciclarProveedores();
			tablaReciclar.setItems(reci);
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}

	@FXML
	public void clic_tabla(){

		if( tablaReciclar.getSelectionModel().getSelectedItem() != null ) {
			pro = tablaReciclar.getSelectionModel().getSelectedItem();
			txtNombre.setText(pro.getNombreProveedor());
			pro.getId_proveedor();
		}
	}

	@FXML
	public void clic_restaurar(){

		try {
			if( txtNombre.getText().trim().isEmpty() ) {
				lblMensaje.setText("Caja de texto vacia");
			}
			else {
				if( pro.restaurarReciclaje() == true ) {
					pro = new Proveedores();
					pro.getId_proveedor();
					actualizarTabla();
					lblMensaje.setText("Restauraci�n exitosa");
					txtNombre.clear();
				}
				else {
					lblMensaje.setText("No se restauro");
				}
			}
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}

	@FXML
	public void cerrarVentana(){

		contVen.dialogStage.close();
		contVen.asignarCentro("../vista/fxml/vistaProveedores.fxml");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources){

		Proveedores p = new Proveedores();
		try {
			tcNombre.setCellValueFactory(new PropertyValueFactory<Proveedores, String>("nombreProveedor"));
			tcCorreo.setCellValueFactory(new PropertyValueFactory<Proveedores, String>("email"));
			tcTelefono.setCellValueFactory(new PropertyValueFactory<Proveedores, String>("telefono"));
			reci = p.getReciclarProveedores();
			tablaReciclar.setItems(reci);
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}
}
