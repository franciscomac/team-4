
package controlador;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.BorderPane;
import modelo.Clientes;
import modelo.Proveedores;
import modelo.Reporte;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class ControladorReporteProveedores implements Initializable {

	private ControladorVentanas	ventanas;
	private Proveedores			pr;
	private Reporte				reporte;
	private BorderPane			contenedor;
	@FXML
	ComboBox<String>			cbEstado;
	@FXML
	ComboBox<Proveedores>		cbContacto, cbProveedor;
	@FXML
	RadioButton					rbActivos, rbInactivo;
	private boolean				status;
	private String				contacto, estado;
	ObservableList<String>		listComboboxEstados	= FXCollections.observableArrayList("Aguascalientes", "Baja california norte", "Baja california sur", "Campeche", "Coahuila", "Chiapas", "Chihuahua", "Durango", "Mexico df", "Guanajuato", "Guerrero", "Hidalgo", "Jalisco", "Michoacan", "Morelos",
			"Mexico,df", "Nayarit", "Nuevo leon", "Oaxaca", "Puebla", "Queretaro", "Quintana roo", "San luis potosi", "Sinaloa", "Sonora", "Tabasco", "Tamaulipas", "Tlaxcala", "Veracruz", "Yucatan", "Zacatecas");

	public ControladorReporteProveedores() {
		ventanas = ventanas.getInstancia();
		pr = new Proveedores();
		reporte = new Reporte();
		contacto = estado = new String();
		status = true;
		contenedor = ventanas.contenedorDialog;
	}

	////////// ***********************************\\\\\\\\\\\\\\\\\\\
	public String comboEstado(){

		if( cbEstado.getSelectionModel().getSelectedItem() != null ) {
			pr = new Proveedores();
			pr.setEstado(cbEstado.getSelectionModel().getSelectedItem());
			estado = pr.getEstado();
			System.out.println(estado);
		}
		else {
			estado = "";
		}
		return estado;
	}

	public String comboContacto(){

		if( cbContacto.getSelectionModel().getSelectedItem() != null ) {
			pr = new Proveedores();
			pr = cbContacto.getSelectionModel().getSelectedItem();
			contacto = pr.getC_nombreContacto();
			System.out.println(contacto);
		}
		else {
			contacto = "";
		}
		return contacto;
	}

	public Boolean statusProveedor(){

		if( rbActivos.isSelected() ) {
			status = true;
		}
		else
			if( rbInactivo.isSelected() ) {
				status = false;
			}
		return status;
	}

	////////// ***********************************\\\\\\\\\\\\\\\\\\\
	@FXML
	public void limpiar(){

		cbEstado.setValue("Seleccionar Estado");
		cbContacto.getSelectionModel().clearSelection();
		cbContacto.promptTextProperty().setValue("Seleccionar contacto");
		rbActivos.setSelected(true);
		rbInactivo.setSelected(false);
	}

	@FXML
	public void click_rbActivos(){

		rbInactivo.setSelected(false);
	}

	@FXML
	public void click_rbInactivos(){

		rbActivos.setSelected(false);
	}

	@FXML
	public void generar(){

		reporte.cargarProveedore("src/vista/reportes/proveedor.jrxml", statusProveedor(), comboContacto(), comboEstado());
		reporte.mostrarReporte();
		limpiar();
	}

	@FXML
	public void cerrarVentana(){

		ventanas.dialogStage.close();
		contenedor.setEffect(null);
		// ventanas.asignarCentro("../vista/fxml/vistaProveedores.fxml");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources){

		try {
			rbActivos.setSelected(true);
			cbEstado.setItems(listComboboxEstados);
			cbEstado.setValue("Seleccionar Estado");
			cbContacto.setItems(pr.getNombreContacto());
			cbContacto.promptTextProperty().setValue("Seleccionar contacto");
		}
		catch( Exception e ) {
			// TODO: handle exception
		}
	}
}
