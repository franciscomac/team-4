
package controlador;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Pair;
import modelo.Conexion;
import modelo.CopiayRestaruacionBD;
import modelo.Usuario;
import vista.Principal;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */

public class ControladorLogin implements Initializable {

	// #region de atributos
	private Conexion			con;
	private Usuario				modeloUsuario;
	private ControladorVentanas	ventanas;
	private GridPane			grid				= new GridPane();
	private String				usernameResult, passwordResult;
	private TextField			username			= new TextField();
	private PasswordField		password			= new PasswordField();
	private Callback			myCallback;
	private Stage				stage;
	public static String		autentificacion;
	private File				archivo;
	private BufferedWriter		escribir;
	private BufferedReader		leer;
	private Encriptar			encriptar;
	private ControladorVentanas	controladorVentanas;
	// modal dialog to get user ID and password
	static String[]				ConnectOptionNames	= { "Login", "Cancel" };
	static String				ConnectTitle		= "Gradesheet connection";
	@FXML
	TextField					txtBaseDatos, txtIP, txtPuerto;
	@FXML
	TitledPane					tpServidor, tpLogin;
	@FXML
	Accordion					accLogin;
	@FXML
	TextField					txtUser;
	@FXML
	PasswordField				txtCon;
	@FXML
	private Label				lblSalida;
	@FXML
	private Label				lblUsuario, lblMensaje;
	@FXML
	private TextField			txtUsuario, txtUsuarioo;
	@FXML
	private Label				lblContrase�a;
	@FXML
	private PasswordField		txtContrasenia, txtContraseniaa;
	@FXML
	private Button				btnConectar;
	// #endregion

	public ControladorLogin() {
		autentificacion = System.getProperty("user.dir") + "\\src\\vista\\Base_de_datos\\autentificacion.rossma";
		archivo = new File(autentificacion);
		encriptar = new Encriptar();
		controladorVentanas = controladorVentanas.getInstancia();
		con = con.getInstancia();
	}

	/*
	 * M�todo asociado al bot�n conectar para establecer conexi�n
	 */
	@FXML
	public void click_btnConectar(){

		con = con.getInstancia();
		if( txtBaseDatos.getText().isEmpty() == false & txtUsuarioo.getText().isEmpty() == false & txtContraseniaa.getText().isEmpty() == false & txtIP.getText().isEmpty() == false & txtPuerto.getText().isEmpty() == false ) {
			// Se asigna datos a los atributos
			con.setUsuario(txtUsuario.getText().trim());
			con.setBd(txtBaseDatos.getText().trim());
			con.setContrasenia(txtContrasenia.getText().trim());
			;
			con.setIp(txtIP.getText().trim());
			con.setPuerto(txtPuerto.getText().trim());
		}
		String mensaje = con.conectar();
		lblMensaje.setText(mensaje);
	}

	@FXML
	public void click_titledServidor(){

		// if (archivo.exists()){
		//
		//
		//
		// // Connect or quit
		// if(JOptionPane.showOptionDialog(null, connectionPanel,
		// ConnectTitle,
		// JOptionPane.OK_CANCEL_OPTION,
		// JOptionPane.INFORMATION_MESSAGE,
		// null, ConnectOptionNames,
		// ConnectOptionNames[0]) != 0)
		// {
		// System.exit(0);
		// }
		// System.out.println( userNameField.getText());
		// System.out.println(passwordField.getText());
		//
		// }
		//
		//
		// JTextField username = new JTextField();
		// JTextField password = new JPasswordField();
		// Object[] message = {
		// "Username:", username,
		// "Password:", password
		// };
		//
		// int option = JOptionPane.showConfirmDialog(null, message, "Login",
		// JOptionPane.OK_CANCEL_OPTION);
		// if (option == JOptionPane.OK_OPTION) {
		// if (username.getText().equals("h") && password.getText().equals("h"))
		// {
		// System.out.println("Login successful");
		// } else {
		// System.out.println("login failed");
		// }
		// } else {
		// System.out.println("Login canceled");
		// }
		// }
		try {
			escribir = new BufferedWriter(new FileWriter(archivo));
			leer = new BufferedReader(new FileReader(archivo));
			if( archivo.exists() ) {
				tpServidor.setExpanded(false);
				// Create the custom dialog.
				Dialog<Pair<String, String>> dialog = new Dialog<>();
				dialog.setTitle("Autentificaci�n");
				dialog.setHeaderText("Favor de iniciar cesi�n como administrador");
				// Set the icon (must be included in the project).
				// dialog.setGraphic(new
				// ImageView(this.getClass().getResource("login.png").toString()));
				// Set the button types.
				ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
				dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);
				// Create the username and password labels and fields.
				GridPane grid = new GridPane();
				grid.setHgap(10);
				grid.setVgap(10);
				grid.setPadding(new Insets(20, 150, 10, 10));
				TextField username = new TextField();
				username.setPromptText("Usuario");
				PasswordField password = new PasswordField();
				password.setPromptText("Contrase�a");
				grid.add(new Label("Usuario:"), 0, 0);
				grid.add(username, 1, 0);
				grid.add(new Label("Contrase�a:"), 0, 1);
				grid.add(password, 1, 1);
				// Habilitar bot�n de acceso / desactivar en funci�n de si se ha
				// introducido un nombre de usuario .
				Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
				loginButton.setDisable(true);
				// Haga un poco de validaci�n (utilizando la sintaxis lambda
				// Java 8
				// ) . .
				username.textProperty().addListener((observable, oldValue, newValue) -> {
					loginButton.setDisable(newValue.trim().isEmpty());
				});
				dialog.getDialogPane().setContent(grid);
				dialog.initModality(Modality.WINDOW_MODAL);
				// dialog.showAndWait();
				// Solicitud enfoque en el campo nombre de usuario por defecto.
				Platform.runLater(() -> username.requestFocus());
				// Convertir el resultado a un par nombre de usuario -
				// contrase�a
				// cuando se hace clic en el bot�n de inicio de sesi�n.
				dialog.setResultConverter(dialogButton -> {
					if( dialogButton == loginButtonType ) {
						return new Pair<>(username.getText(), password.getText());
					}
					return null;
				});
				Optional<Pair<String, String>> result = dialog.showAndWait();
				modeloUsuario = new Usuario();
				modeloUsuario.setNombre(username.getText());
				modeloUsuario.setContrasenia(password.getText());
				if( modeloUsuario.Existe() ) {
					escribir.write(encriptar.encriptar(modeloUsuario.getPrivilegio(), "rossma"));
					escribir.close();
					if( encriptar.desencriptar(leer.readLine(), "rossma").equals("Administrador") ) {
						leer.close();
						escribir = new BufferedWriter(new FileWriter(archivo));
						;
						escribir.write(encriptar.encriptar("empleado", "rossma"));
						escribir.close();
						result.ifPresent(usernamePassword -> {
						});
						controladorVentanas.asignarEmergente("../vista/fxml/configurarServidor.fxml", "Configuraci�n de Servidor");
					}
					else {
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Alerta");
						alert.setHeaderText("Los campos no coinciden con un adminstrador");
						alert.showAndWait();
					}
				}
				else {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Alerta");
					alert.setHeaderText("Ninguna coincidencia encontrada");
					alert.setContentText("Favor de verificar los campos");
					// dialog.showAndWait();
					alert.showAndWait();
				}
			}
			else {
				escribir.write(encriptar.encriptar("empleado", "rossma"));
				escribir.close();
			}
		}
		catch( IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void dialogAcceso(){

		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(0, 10, 0, 10));
		username.setPromptText("Username");
		password.setPromptText("Password");
		grid.add(new Label("Username:"), 0, 0);
		grid.add(username, 1, 0);
		grid.add(new Label("Password:"), 0, 1);
		grid.add(password, 1, 1);
		myCallback = new Callback() {

			@Override
			public Object call(Object arg0){

				usernameResult = username.getText();
				passwordResult = password.getText();
				return null;
			}
		};
	}

	@FXML
	public void validarUsuario(){

		// Accesar
		if( txtUsuario.getText().isEmpty() | txtContrasenia.getText().isEmpty() ) {
			lblMensaje.setText("Faltan datos por ingresar.");
		}
		else {
			// Validar si existe el usuario
			if( modeloUsuario == null ) {
				modeloUsuario = new Usuario();
			}
			// Asignamos los datos
			modeloUsuario.setNombre(txtUsuario.getText());
			modeloUsuario.setContrasenia(txtContrasenia.getText());
			// Verificamos si existe en la BD
			boolean resultado = modeloUsuario.Existe();
			if( resultado ) {
				ventanas = ControladorVentanas.getInstancia();
				ventanas.setPrimaryStage(Principal.getPrimaryStage());
				ventanas.asignarEscena("../vista/fxml/Principal.fxml", "Menu", modeloUsuario.getPrivilegio(), modeloUsuario.getNombre());
			}
			else {
				lblMensaje.setText("Usuario no valido");
				System.out.println("Usuario no v�lido");
			}
		}
	}

	public void verificarBase(){

		// Usuario con = new Usuario();
		modeloUsuario = new Usuario();
		modeloUsuario.baseExiste();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1){

		con.reestablecerDatosDeConexion(false);
		// verificarBase();
		CopiayRestaruacionBD copia = new CopiayRestaruacionBD();
		copia.baseExiste();
	}
}
