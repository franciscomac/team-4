
package controlador;

import java.net.URL;
import java.util.ResourceBundle;
import modelo.Clientes;
import modelo.ClientesFisicos;
import modelo.Productos;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * 29 de sept. de 2015
 * 
 * @ROSSMA Fco. Javier Machuca Rojas H�ctor Ren� Espinoza G�mez Luis Angel
 *         Fernndez Vasquez
 */
public class ControladorReciclajeClientes implements Initializable {

	private Clientes				cl;
	private ControladorErrores		error;
	ControladorVentanas				conVtnas;
	@FXML
	TextField						txtNombre;
	@FXML
	TableView<Clientes>				tablaClientes;
	@FXML
	TableColumn<Clientes, String>	tcRfc, tcNombre;
	@FXML
	ObservableList<Clientes>		reci;
	@FXML
	ObservableList<Clientes>		rest;

	public ControladorReciclajeClientes() {
		cl = new Clientes();
		error = new ControladorErrores();
		conVtnas = ControladorVentanas.getInstancia();
		reci = FXCollections.observableArrayList();
		rest = FXCollections.observableArrayList();
	}

	public ObservableList<Clientes> restaurar(){

		rest = FXCollections.observableArrayList();
		Clientes c = new Clientes();
		c.setCodigo_cliente(new SimpleIntegerProperty());
		rest.add(c);
		return rest;
	}

	public void actualizarTabla(){

		try {
			cl = new Clientes();
			reci = cl.getReciclaNombre();
			tablaClientes.setItems(reci);
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}

	@FXML
	public void click_tabla(){

		if( tablaClientes.getSelectionModel().getSelectedItem() != null ) {
			cl = tablaClientes.getSelectionModel().getSelectedItem();
			txtNombre.setText(cl.getNombresCombo().toString());
		}
	}

	@FXML
	public void clic_restaurar(){

		try {
			if( txtNombre.getText().trim().isEmpty() ) {
				System.out.println("faltan datos");
			}
			else {
				if( cl.restaurarReciclaje() == true ) {
					cl = new Clientes();
					cl.getCodigo_cliente();
					actualizarTabla();
					txtNombre.clear();
					System.out.println("se restauro");
				}
			}
		}
		catch( Exception e ) {
			// TODO: handle exception
		}
	}

	@FXML
	public void cerrar(){

		conVtnas.dialogStage.close();
		// conVtnas.setPrimaryStage(primaryStage);
		conVtnas.asignarCentro("../vista/fxml/vistaClientes.fxml");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources){

		Clientes cl = new Clientes();
		try {
			tcNombre.setCellValueFactory(new PropertyValueFactory<Clientes, String>("nombresCombo"));
			// tcNombre.setCellValueFactory(new PropertyValueFactory<Clientes,
			// String>("F"));
			reci = cl.getReciclaNombre();
			tablaClientes.setItems(reci);
		}
		catch( Exception e ) {
			System.out.println(e);
			e.printStackTrace();
		}
	}
}
