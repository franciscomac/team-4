
package controlador;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Timer;
import javax.swing.JOptionPane;
import com.sun.prism.paint.Color;
import modelo.ClientesFisicos;
import modelo.Reporte;
import vista.Principal;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class ControladorPrincipal implements Initializable {

	/*
	 * Region de los atributos
	 */
	@FXML
	private Button				btnClientes, btnPrueba, btnProductos, btnVentas, btnProveedor, btnStock, btnSalir, btnBase, btnNuevoUsuario;
	@FXML
	private Label				lblModulo, lblFecha, lblHora, lblMinutos;
	@FXML
	private MenuItem			reportClientes, atajoCliente, atajoProveedor, atajoProducto, atajoVenta, atajoCerrar, itemRespaldo;
	private Reporte				r;
	public static String		nivel;
	public static String		nombre;
	private ControladorVentanas	ventanas;
	private Principal			principal;

	/*
	 * Region del constructor
	 */
	public ControladorPrincipal() {
		principal = new Principal();
		ventanas = ControladorVentanas.getInstancia();
		r = new Reporte();
	}

	/*
	 * Metodo para teclas rapidas
	 */
	public void atajosTeclas(){

		reportClientes.setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN));
		atajoCliente.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN));
		atajoProducto.setAccelerator(new KeyCodeCombination(KeyCode.D, KeyCombination.CONTROL_DOWN));
		atajoProveedor.setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN));
		atajoVenta.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN));
		atajoCerrar.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
	}

	public void fecha(){

		Date d = new Date();
		LocalDate ahora = LocalDate.now();
		String hora = Integer.toString(d.getHours());
		String minutos = Integer.toString(d.getMinutes());
		String fecha = String.valueOf(ahora);
		// txtFecha.setText(fecha);
		lblFecha.setText(fecha);
		// lblHora.setText(hora);
		// lblMinutos.setText(minutos);
	}

	/*
	 * public void HoraActual(){ int j=1000; Timer timer = new Timer (j, new
	 * ActionListener (){ public void actionPerformed(ActionEvent
	 * e){lbhora.setText(conectar.MostrarHora());}}); timer.start(); } //este es
	 * le metodo para mostrar la hora public String MostrarHora(){ String hora=
	 * Cal.get(Cal.HOUR_OF_DAY)+":"+Cal.get(Cal.MINUTE)+":"+Cal.get(Cal.SECOND);
	 * return hora; }
	 */
	@FXML
	public void archivoPDF(){

		try {
			File path = new File("src/vista/pdf/MANUAL_USUARIO.pdf");
			Desktop.getDesktop().open(path);
		}
		catch( IOException ex ) {
			ex.printStackTrace();
		}
	}

	/*
	 * Metodo boton para el modulo Clientes
	 */
	@FXML
	public void clientes(){

		ventanas.asignarCentro("../vista/fxml/vistaClientes.fxml");
		lblModulo.setText("CLIENTES");
		btnClientes.requestFocus();
		btnClientes.setStyle("-fx-background-color: #0000;");
		btnVentas.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnProductos.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnProveedor.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnNuevoUsuario.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnStock.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
	}

	/*
	 * Metodo boton para el modulo Proveedores
	 */
	@FXML
	public void proveedores(ActionEvent event){

		ventanas.asignarCentro("../vista/fxml/vistaProveedores.fxml");
		lblModulo.setText("PROVEEDORES");
		btnProveedor.requestFocus();
		btnProveedor.setStyle("-fx-background-color: #0000;");
		btnVentas.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnProductos.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnNuevoUsuario.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnStock.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnClientes.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
	}

	/*
	 * Metodo boton para el modulo Productos
	 */
	@FXML
	public void productos(ActionEvent event){

		ventanas.asignarCentro("../vista/fxml/vistaProductos.fxml");
		lblModulo.setText("PRODUCTOS");
		btnProductos.requestFocus();
		btnProductos.setStyle("-fx-background-color: #0000;");
		btnVentas.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnProveedor.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnNuevoUsuario.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnStock.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnClientes.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
	}

	/*
	 * Metodo boton para el modulo Ventas
	 */
	@FXML
	public void ventas(ActionEvent event){

		ventanas.asignarCentro("../vista/fxml/vistaVentas.fxml");
		lblModulo.setText("VENTAS");
		btnVentas.requestFocus();
		btnVentas.setStyle("-fx-background-color: #0000;");
		btnProductos.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnProveedor.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnNuevoUsuario.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnStock.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnClientes.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
	}

	/*
	 * Metodo boton para el modulo Stock
	 */
	@FXML
	public void stock(ActionEvent event){

		ventanas.asignarCentro("../vista/fxml/vistaStock.fxml");
		lblModulo.setText("STOCK");
		btnStock.requestFocus();
		btnStock.setStyle("-fx-background-color: #0000;");
		btnVentas.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnProductos.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnProveedor.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnNuevoUsuario.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnClientes.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
	}

	/*
	 * Metodo boton para el modulo Usuarios
	 */
	@FXML
	public void usuarios(ActionEvent event){

		ventanas.asignarCentro("../vista/fxml/vistaNuevoUsuario.fxml");
		lblModulo.setText("USUARIOS");
		btnNuevoUsuario.requestFocus();
		btnNuevoUsuario.setStyle("-fx-background-color: #0000;");
		btnVentas.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnProductos.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnProveedor.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnStock.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
		btnClientes.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(76,76,76,1) 0%, rgba(43,43,43,1) 0%, rgba(89,89,89,1) 16%, rgba(102,102,102,1) 28%, rgba(71,71,71,1) 48%, rgba(19,19,19,1) 100%);");
	}

	/*
	 * Metodo boton para �quienes somos?
	 */
	@FXML
	public void desarrolladores(){

		ventanas.asignarEmergente("../vista/fxml/vistaDesarrolladores.fxml", "DESARROLLADORES");
	}

	/*
	 * Metodo boton para respaldar base de datos
	 */
	@FXML
	public void respaldo(){

		Backup respaldo = new Backup();
		respaldo.setVisible(true);
		respaldo.setSize(500, 500);
		respaldo.setLocationRelativeTo(null);
	}

	/*
	 * Metodo boton para ayuda al usuario
	 */
	@FXML
	public void ayuda(){

		String ruta = "../vista/pdf/lectura.pdf";
		try {
			File path = new File(ruta);
			Desktop.getDesktop().open(path);
		}
		catch( IOException ex ) {
			ex.printStackTrace();
			System.out.println("error");
		}
	}

	/*
	 * Metodo boton para cerrar sesion de la aplicacion
	 */
	@FXML
	public void cerrarSesion() throws Exception{

		ventanas.asignarEmergente("../vista/fxml/cerrarSesion.fxml", "Confirmaci�n");
	}

	/*
	 * Metodo region de los reportes
	 */
	@FXML
	public void reporte_Clientes(){

		ventanas.asignarEmergente("../vista/fxml/vistaReportes.fxml", "Reportes");
		// r.cargarCliente("src/vista/reportes/Clientes.jrxml"," ");
		// r.mostrarReporte();
	}

	@FXML
	public void reporte_Proveedores(){

		ventanas.asignarEmergente("../vista/fxml/vistaReporteProveedor.fxml", "Reporte Proveedores");
		// r.cargarReporte("src/vista/reportes/Proveedores.jrxml");
		// r.mostrarReporte();
	}

	@FXML
	public void reporte_Ventas(){

		ventanas.asignarEmergente("../vista/fxml/vistaReportesVentas.fxml", "Reporte Ventas");
	}

	/*
	 * Metodo para inicializar el controlador
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1){

		fecha();
		atajosTeclas();
		ventanas = ControladorVentanas.getInstancia();
		lblModulo.setText("PRINCIPAL");
		if( ControladorPrincipal.nivel.equals("administrador") ) {
			// btnClientes.setDisable(false);
		}
		else
			if( ControladorPrincipal.nivel.equals("empleado") ) {
				// btnBase.setDisable(true);
				btnClientes.setDisable(true);
				btnNuevoUsuario.setDisable(true);
				btnProveedor.setDisable(true);
				itemRespaldo.setDisable(true);
				atajoCliente.setDisable(true);
				atajoProveedor.setDisable(true);
				reportClientes.setDisable(true);
			}
	}
}
