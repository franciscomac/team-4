
package controlador;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.BorderPane;
import modelo.Reporte;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class ControladorReportes implements Initializable {

	@FXML
	RadioButton					rbFisico, rbMoral, rbActivo, rbInactivo;
	private boolean				status;
	private String				tipo;
	private Reporte				repor;
	private ControladorVentanas	ventanas;
	private BorderPane			contenedor;

	public ControladorReportes() {
		status = true;
		tipo = "Fisico";
		repor = new Reporte();
		ventanas = ControladorVentanas.getInstancia();
		contenedor = ventanas.contenedorDialog;
	}

	public String tipoCliente(){

		if( rbFisico.isSelected() ) {
			tipo = "Fisico";
		}
		else
			if( rbMoral.isSelected() ) {
				tipo = "Moral";
			}
		return tipo;
	}

	public Boolean statusCliente(){

		if( rbActivo.isSelected() ) {
			status = true;
		}
		else
			if( rbInactivo.isSelected() ) {
				status = false;
			}
		return status;
	}

	@FXML
	public void click_rbFisico(){

		rbMoral.setSelected(false);
		;
	}

	@FXML
	public void click_rbMoral(){

		rbFisico.setSelected(false);
	}

	@FXML
	public void click_rbActivos(){

		rbInactivo.setSelected(false);
	}

	@FXML
	public void click_rbInactivos(){

		rbActivo.setSelected(false);
	}

	@FXML
	public void generarReporte(){

		repor.cargarCliente("src/vista/reportes/Clientes.jrxml", tipoCliente(), statusCliente());
		repor.mostrarReporte();
	}

	@FXML
	public void cerrarVentana(){

		ventanas.dialogStage.close();
		contenedor.setEffect(null);
		// ventanas.asignarCentro("../vista/fxml/vistaClientes.fxml");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources){

		// TODO Auto-generated method stub
	}
}
