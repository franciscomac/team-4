
package controlador;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.BorderPane;
import modelo.Clientes;
import modelo.Reporte;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class ControladorReporteVentas implements Initializable {

	private ControladorVentanas	ventanas;
	private Reporte				reporte;
	private Clientes			cl;
	private ControladorErrores	error;
	private BorderPane			contenedor;
	private LocalDate			fecha_Inicial;
	private LocalDate			fecha_Final;
	@FXML
	ComboBox<Clientes>			cbCliente;
	@FXML
	ComboBox<String>			cbTipo;
	@FXML
	DatePicker					dpInicial, dpFinal;
	@FXML
	CheckBox					ckCliente, ckFecha;
	private String				tipo, fecha;
	private int					codigoCliente;
	ObservableList<String>		tipoCliente	= FXCollections.observableArrayList("Fisico", "Moral");

	public ControladorReporteVentas() {
		error = new ControladorErrores();
		ventanas = ControladorVentanas.getInstancia();
		reporte = new Reporte();
		cl = new Clientes();
		contenedor = ventanas.contenedorDialog;
		tipo = "Fisico";
		fecha = new String();
		codigoCliente = 1;
	}

	@FXML
	public void limpiar(){

		cbCliente.getSelectionModel().selectFirst();
		dpInicial.getEditor().clear();
		dpFinal.getEditor().clear();
	}

	@FXML
	public String comboTipo(){

		if( cbTipo.getSelectionModel().getSelectedItem() != null ) {
			if( cbTipo.getSelectionModel().equals("Fisico") ) {
				tipo = "Fisico";
			}
			else {
				tipo = "Moral";
			}
		}
		System.out.println("tipo " + tipo);
		return tipo;
	}

	@FXML
	public int codigo_Cliente(){

		if( cbCliente.getSelectionModel().getSelectedItem() != null ) {
			cl = cbCliente.getSelectionModel().getSelectedItem();
			codigoCliente = cl.getCodigo_cliente();
			System.out.println("se selecciono cliente");
		}
		else {
		}
		System.out.println("codigo cliente " + codigoCliente);
		return codigoCliente;
	}

	/*
	 * @FXML public LocalDate fechaInicial(){ fecha_Inicial =
	 * dpInicial.getValue(); System.out.println(fecha_Inicial); return
	 * fecha_Inicial;
	 * 
	 * }
	 * 
	 * @FXML public LocalDate fechaFinal(){ fecha_Final = dpFinal.getValue();
	 * System.out.println(fecha_Final); return fecha_Final;
	 * 
	 * }
	 */
	public int recuperarId(){

		int i = 0;
		Clientes c = new Clientes();
		i = c.getCodigo_cliente();
		System.out.println("recuperar ID " + i);
		return i;
	}

	
	 @FXML boolean check_Todos(){ boolean bandera = false;
	 if(ckCliente.isSelected()){
	 
	 cbCliente.setDisable(true); codigoCliente = 1; bandera = true;
	 System.out.println("codigo "+codigoCliente); 
	 }else{
	 cbCliente.setDisable(false);
	 bandera = false; 
	 } 
	 return bandera; 
	 }
	 
	/*
	 * @FXML public void check_Fecha(){ if(ckFecha.isSelected()){
	 * dpInicial.setDisable(false); dpFinal.setDisable(false); }else{
	 * dpInicial.setDisable(true); dpFinal.setDisable(true); } }
	 * 
	 * @FXML public String click_FechaInicial(){ String pattern = "yyyy-MM-dd";
	 * DateTimeFormatter df = DateTimeFormatter.ofPattern(pattern); String
	 * inicio = df.format(dpInicial.getValue()); //String fin =
	 * df.format(dpFinal.getValue()); System.out.println("fecha inicio "
	 * +inicio);
	 * 
	 * return inicio; }
	 * 
	 * @FXML public String click_FechaFinal(){ String pattern = "yyyy-MM-dd";
	 * DateTimeFormatter df = DateTimeFormatter.ofPattern(pattern); //String
	 * inicio = df.format(dpInicial.getValue()); String fin =
	 * df.format(dpFinal.getValue()); System.out.println("fecha final "+fin);
	 * return fin; }
	 */
	@FXML
	public void generar(){
		if(check_Todos()){
			String pattern = "yyyy-MM-dd";
			DateTimeFormatter df = DateTimeFormatter.ofPattern(pattern);
			String inicio = df.format(dpInicial.getValue());
			String fin = df.format(dpFinal.getValue());
			System.out.println("fecha inicio " + inicio);
			reporte.cargarVentasTodos("src/vista/reportes/ventasTodos.jrxml",  inicio, fin);
			reporte.mostrarReporte();
		}else{
			try {
				String pattern = "yyyy-MM-dd";
				DateTimeFormatter df = DateTimeFormatter.ofPattern(pattern);
				String inicio = df.format(dpInicial.getValue());
				String fin = df.format(dpFinal.getValue());
				System.out.println("fecha inicio " + inicio);
				reporte.cargarVentas("src/vista/reportes/ventas.jrxml", codigo_Cliente(), inicio, fin);
				reporte.mostrarReporte();
			}
			catch( Exception e ) {
				e.printStackTrace();
				error.printLong(e.getMessage(), this.getClass().toString());
			}
		}

		
	}

	@FXML
	public void cerrar_ventana(){

		ventanas.dialogStage.close();
		contenedor.setEffect(null);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1){

		try {
			cbCliente.setItems(cl.getNombreCombo());
			cbCliente.getSelectionModel().selectFirst();
			codigoCliente = 1;
			// cbCliente.setDisable(true);
			// cbTipo.setItems(tipoCliente);
			// cbTipo.getSelectionModel().selectFirst();
			// dpInicial.setDisable(true);
			// dpFinal.setDisable(true);
		}
		catch( Exception e ) {
			e.printStackTrace();
			error.printLong(e.getMessage(), this.getClass().toString());
		}
	}
}
