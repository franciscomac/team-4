
package controlador;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */

public class ControladorDesarrolladores implements Initializable {

	private ControladorVentanas	conventanas;
	private BorderPane			contenedor;

	public ControladorDesarrolladores() {
		conventanas = ControladorVentanas.getInstancia();
		contenedor = conventanas.contenedorDialog;
	}

	@FXML
	public void cerrarVentana(){

		conventanas.dialogStage.close();
		contenedor.setEffect(null);
		// ventanas.asignarCentro("../vista/fxml/vistaClientes.fxml");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources){

		// TODO Auto-generated method stub
	}
}
