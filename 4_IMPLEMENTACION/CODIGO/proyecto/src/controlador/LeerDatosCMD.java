
package controlador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
/**
 * 02/12/2015
 * @ROSSMA
 * Fco. Javier Machuca Rojas
 * H�ctor Ren� Espinoza G�mez
 * Luis Angel Fernndez Vasquez
 */
public class LeerDatosCMD implements Runnable {

	private final InputStream flujoDeLaEntrada;

	public LeerDatosCMD(InputStream inputStream) {
		this.flujoDeLaEntrada = inputStream;
	}

	@Override
	public void run(){

		BufferedReader br = new BufferedReader(new InputStreamReader(flujoDeLaEntrada));
		String linea = "";
		try {
			while( (linea = br.readLine()) != null ) {
				System.out.println(linea);
			}
		}
		catch( IOException e ) {
			e.printStackTrace();
		}
	}
}
